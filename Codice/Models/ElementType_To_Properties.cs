using System;
using System.Collections.Generic;

namespace Codice.Models
{
    public partial class ElementType_To_Properties
    {
        public string ElementType { get; set; }
        public string PropertyCode { get; set; }
    }
}
