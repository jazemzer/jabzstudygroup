﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace DSStacks
{
    public class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
    }

    public class Program
    {
        public static void Main()
        {

            #region Pushing a reference object into stack just copies the reference 
            
            Person p = new Person() {Age = 34, Name = "Vaibav"};
            Stack<Person> stack1 = new Stack<Person>();
            stack1.Push(p);

            Stack<Person> stack2 = new Stack<Person>();
            stack2.Push(p);
            
            Person popped = stack1.Pop();
            popped.Age = 23;

            Console.WriteLine(stack2.Pop().Age);
            #endregion

            #region Balanced Parameters Checker

            string sampleInput = "(()([{}]{}))"; //Valid Input
            Console.Write(AreAllParmetersBalanced(sampleInput));
            #endregion



            Console.Read();
        }

        private string CovnertBase(string decimalInput)
        {

            return string.Empty;
        }

        private static bool AreAllParmetersBalanced(string input)
        {
            bool balanced = true;

            Stack<char> myStack = new Stack<char>();

            string possibleOpeningChars = "({[";
            string possibleClosingChars = ")}]";

            foreach (var character in input)
            {
                int charIndex = possibleClosingChars.IndexOf(character);
                if (charIndex >-1)
                {
                    if (myStack.Count == 0) //If Empty, avoid throwing exception while popping
                    {
                        balanced = false;
                        break;
                    }
                    else
                    {
                        var findOpening = myStack.Pop();
                        if (possibleOpeningChars[charIndex] != findOpening)
                        {
                            balanced = false;
                            break;
                        }
                    }
                }
                else
                {
                    myStack.Push(character);
                }
            }
            return balanced && myStack.Count == 0 ? true : false;

        }
    }
    





}
