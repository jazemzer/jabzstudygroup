using System;
using System.Collections.Generic;

namespace Codice.Models
{
    public partial class Property_Info
    {
        public string PropertyCode { get; set; }
        public string PropertyName { get; set; }
        public string PropertyType { get; set; }
    }
}
