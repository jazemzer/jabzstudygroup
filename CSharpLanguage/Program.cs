﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpLanguage
{
    class Program
    {
        static void Main(string[] args)
        {
            OldThreading.TestScenarios();
           CapturedVariables.TestScenarios();
            OverloadResolution.TestScenarios();
            Structs.TestScenarios();

            #region checked and unchecked statements
            checked
            {
                unchecked
                {
                    Console.WriteLine(int.MaxValue + 1);
                }
            }
            #endregion



            //Console.Read();
        }
    }

  


}
