using System;
using System.Collections.Generic;

namespace Codice.Models
{
    public partial class Element_Info
    {
        public Element_Info()
        {
            this.Element_Properties = new List<Element_Properties>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string ElementType { get; set; }
        public virtual ICollection<Element_Properties> Element_Properties { get; set; }
    }
}
