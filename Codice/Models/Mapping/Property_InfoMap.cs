using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Codice.Models.Mapping
{
    public class Property_InfoMap : EntityTypeConfiguration<Property_Info>
    {
        public Property_InfoMap()
        {
            // Primary Key
            this.HasKey(t => t.PropertyCode);

            // Properties
            this.Property(t => t.PropertyCode)
                .IsRequired()
                .HasMaxLength(5);

            this.Property(t => t.PropertyName)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.PropertyType)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Property_Info");
            this.Property(t => t.PropertyCode).HasColumnName("PropertyCode");
            this.Property(t => t.PropertyName).HasColumnName("PropertyName");
            this.Property(t => t.PropertyType).HasColumnName("PropertyType");
        }
    }
}
