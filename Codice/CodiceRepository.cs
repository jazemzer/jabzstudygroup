﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Codice.Models;

namespace Codice
{
    public interface ICodeRepository
    {
        
    }

    public class CodiceRepository : ICodeRepository
    {
        private CodiceContext dbContext;
        public CodiceRepository()

        {
            dbContext = new CodiceContext();
        }

        public Element_Info FindById(int id)
        {
            return dbContext.Element_Info.Find(id);
        }

        public List<Element_Info> FindByType(string elementType)
        {
            return dbContext.Element_Info.Where(x => x.ElementType == elementType).ToList();
        }

        public List<Element_Info> FindByTypeAndPropertyInfo(string elementType, string propertyCode, string propertyValue)
        {
            return dbContext.Element_Info.Where(
                    x =>
                        x.ElementType == elementType &&
                        x.Element_Properties.Any(p => p.PropertyCode == propertyCode && p.PropertyValue == propertyValue)).ToList();
        }
    }
}
