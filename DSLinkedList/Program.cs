﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSLinkedList
{
    class Program
    {
        static void Main(string[] args)
        {                                          
            #region Skip K elements and Reverse 

            /* Given number k, for Single linked list, skip k nodes and then reverse k nodes, till the end.
             */

            //Hint - http://www.careercup.com/question?id=5838726617890816

            JabzLinkedList<int> skipReverse = new JabzLinkedList<int>();

            for (int i = 1; i <= 11; i++)
            {
                skipReverse.Add(i);
            }

            //SkipAndReverse(skipReverse.Root, 3);
            //SkipAndReverse2(skipReverse.Root, 3);
            SkipAndReverseRecursive(skipReverse.Root, 3 ,true);

            //Printing output
            for (JabzLLNode<int> item = skipReverse.Root; item != null; item = item.Next)
            {
                Console.Write(item.Value + " -> ");
            }

            #endregion
            Console.WriteLine("\r\n*****");



            #region Find Middle Element in a single pass
            //Solution - http://www.geeksforgeeks.org/write-a-c-function-to-print-the-middle-of-the-linked-list/

            JabzLinkedList<int> findMiddle = new JabzLinkedList<int>();

            for (int i = 1; i <= 25; i++)
            {
                findMiddle.Add(i);
            }

            var result = FindMiddleElement(findMiddle.Root);

            Console.WriteLine(result.Value);

            #endregion
            Console.WriteLine("\r\n*****");




            #region Find K-th node from end
            /* Get the Kth node from end of a linked list. It counts from 1 here, so the 1st node from end is the tail of list. 
             * For instance, given a linked list with 6 nodes, whose value are 1, 2, 3, 4, 5, 6, its 3rd node from end is the node with value 4.
             */

            //Solution  http://codercareer.blogspot.in/2011/10/no-10-k-th-node-from-end.html

            JabzLinkedList<int> findElementFromEnd = new JabzLinkedList<int>();

            for (int i = 1; i <= 25; i++)
            {
                findElementFromEnd.Add(i);
            }

            result = FindElementFromEnd(findElementFromEnd.Root, 2);

            Console.WriteLine(result.Value);


            #endregion
            Console.WriteLine("\r\n*****");




            #region Find presence of Loops 

            //Solution - http://codercareer.blogspot.in/2012/01/no-29-loop-in-list.html

            var hasloop = HasLoops(skipReverse.Root);
            Console.WriteLine(hasloop);

            JabzLinkedList<int> loopInput = new JabzLinkedList<int>();

            Random rnd = new Random();
            int pick = rnd.Next(1, 25);

            JabzLLNode<int> loopStart = null;
            JabzLLNode<int> lastAddedNode = null;
            for (int i = 1; i <= 25; i++)
            {
                lastAddedNode = loopInput.Add(i);
                if (i == pick)
                {
                    loopStart = lastAddedNode;
                }
            }

            lastAddedNode.Next = loopStart;


            hasloop = HasLoops(loopInput.Root);
            Console.WriteLine(hasloop);

            #endregion
            Console.WriteLine("\r\n*****");



            #region Reverse Linked list
            
            JabzLinkedList<int> reverseAll = new JabzLinkedList<int>();

            for (int i = 1; i <= 6; i++)
            {
                reverseAll.Add(i);
            }

            ReverseCompleteList(reverseAll);

            //Printing output
            for (JabzLLNode<int> item = reverseAll.Root; item != null; item = item.Next)
            {
                Console.Write(item.Value + " -> ");
            }
            #endregion
            Console.WriteLine("\r\n*****");



            #region Check Palindrome or not
            //Question - http://basicalgos.blogspot.in/2012/06/58-linked-list-related-questions.html

            List<string> inputset = new List<string>()
            {
                "malayalam",
                "aviddiva",
                "unnk",
                "testinpu"
            };
            foreach (var palindrome in inputset)
            {
                JabzLinkedList<char> palindromeList = new JabzLinkedList<char>();
                foreach (var s in palindrome)
                {
                    palindromeList.Add(s);
                }

                bool isPalindrome = CheckPalindrome(palindromeList.Root);

                Console.WriteLine(string.Format("{0} : {1}",palindrome, isPalindrome));
            }

            #endregion
            Console.WriteLine("\r\n*****");



            #region Find entry node of the loop
            /* If there is a loop in a linked list, how to get the entry node of the loop? 
             * The entry node is the first node in the loop from head of list. 
             * For instance, the entry node of loop in the list of Figure 1 is the node with value 3.
             */
            Console.WriteLine(pick);
            result = FindLoopEntryNode(loopInput.Root);
            Console.WriteLine(result.Value);
            #endregion

            Console.Read();
            //Yet to solve - http://basicalgos.blogspot.in/2012/06/58-linked-list-related-questions.html

            //Theory
            // http://www.geeksforgeeks.org/linked-list-vs-array/
        }

        public static JabzLLNode<int> FindLoopEntryNode(JabzLLNode<int> start)
        {
            var P1 = start;
            var P2 = start;

            JabzLLNode<int> entryNode = null;
            JabzLLNode<int> intersectNode = null;
            while (P1 != null)
            {
                if (P1.Next != null
                    && P1.Next.Next != null)
                {
                    P1 = P1.Next.Next;
                    P2 = P2.Next;

                    if (P1 == P2)
                    {
                        intersectNode = P1;
                        break;
                    }
                }
                else
                {
                    break;
                }
            }

            int noOfElementsinLoop = 0;
            if (intersectNode != null)
            {
                do
                {
                    P2 = P2.Next;
                    noOfElementsinLoop++;
                }while(P1 != P2);

                var P3 = start;
                var P4 = start;

                while(noOfElementsinLoop > 0)
                {
                    P4 = P4.Next;
                    noOfElementsinLoop--;
                }

                do{
                    P3 = P3.Next;
                    P4 = P4.Next;
                }while(P3 != P4);

                entryNode = P3;
            }

            return entryNode;

        }

        public static bool CheckPalindrome(JabzLLNode<char> start)
        {
            bool result = true;

            #region Finding Middle element

            var P1 = start;
            var P2 = start;
            bool isOdd = false;
            while (P2 != null)
            {
                if (P2.Next != null)
                {
                    if (P2.Next.Next != null)
                    {
                        P2 = P2.Next.Next;
                        P1 = P1.Next;
                    }
                    else
                    {
                        isOdd = false;
                        break;
                    }
                }
                else
                {
                    isOdd = true;
                    break;
                }
            }
            #endregion

            var middle = P1.Next;

            JabzLLNode<char> prev2prev = null;
            var prev = middle;
            var curr = middle.Next;

            while (curr != null)
            {
                prev.Next = prev2prev;
                prev2prev = prev;
                prev = curr;
                curr = curr.Next;
            }
            prev.Next = prev2prev;

            var lefttHalf = start;
            var reversedRightHalf = prev;

            for (; reversedRightHalf != null; reversedRightHalf = reversedRightHalf.Next, lefttHalf = lefttHalf.Next)
            {
                if (lefttHalf.Value != reversedRightHalf.Value)
                {
                    result = false;
                    break;
                }
            }

            return result;
        }

        public static bool HasLoops(JabzLLNode<int> start)
        {
            bool result = false;

            var P1 = start;
            var P2 = start;

            while (P2 != null)
            {
                if (P2.Next != null
                   && P2.Next.Next != null)
                {
                    P1 = P1.Next;
                    P2 = P2.Next.Next;

                    if (P1 == P2)
                    {
                        result = true;
                        break;
                    }
                }
                else
                {
                    break;
                }
            
            }

            return result;
        }

        public static JabzLLNode<int> FindMiddleElement(JabzLLNode<int> start)
        {
            var current = start;
            var middle = start;
            while (current != null)
            {
                if (current.Next != null
                    && current.Next.Next != null)
                {
                    current = current.Next.Next;
                    middle = middle.Next;
                }
                else
                    break;
            }
            return middle;
        }

        public static void ReverseCompleteList(JabzLinkedList<int> listToReverse)
        {
            JabzLLNode<int> prev2Prev = null;
            var prev = listToReverse.Root;
            var current = listToReverse.Root.Next;
            while (current != null)
            {
                prev.Next = prev2Prev;
                prev2Prev = prev;
                prev = current;
                current = current.Next;
            }
            prev.Next = prev2Prev;
            listToReverse.Root = prev;
        }

        public static void SkipAndReverse2(JabzLLNode<int> start, int k)
        {
            JabzLLNode<int> current = start;
            bool isSkip = true;
            int i = 1;

            while (current != null)
            {
                if (isSkip)
                {
                    i = 1;
                    while (current != null &&  i < k) //The loop runs for K - 1 only so that the end item of skip iteration is held in current
                    {
                        current = current.Next;
                        i++;
                    }
                    isSkip = false;
                }
                else
                {
                    var nextStart = current.Next; //Start with the nK + 1 th item 
                    JabzLLNode<int> p = nextStart;
                    JabzLLNode<int> q = null;
                    JabzLLNode<int> r = null;

                    i = 1;
                    while (p != null && i <= k)
                    {
                        r = q;
                        q = p;
                        p = p.Next;
                        q.Next = r;
                        i++;
                    }

                    current.Next = q;
                    if (nextStart != null)
                        nextStart.Next = p;

                    current = p; 

                    isSkip = true;
                }
            }
        }

        public static JabzLLNode<int> SkipAndReverseRecursive(JabzLLNode<int> start, int k, bool isSkip)
        {

            JabzLLNode<int> current = start;
            JabzLLNode<int> returnNode = start;
            JabzLLNode<int> currentTail = null; //CurrentTail and current refer to the same thing but here for readability 
            JabzLLNode<int> nextStart = null;
            int i = 1;

            if (current != null)
            {
                if (isSkip)
                {
                    i = 1;
                    while (current != null && i < k) //The loop runs for K - 1 only so that the end item of skip iteration is held in current
                    {
                        current = current.Next;
                        i++;
                    }
                    currentTail = current;
                    nextStart = current.Next;
                }
                else
                {
                    currentTail = current;
                    JabzLLNode<int> p = current;
                    JabzLLNode<int> q = null;
                    JabzLLNode<int> r = null;

                    i = 1;
                    while (p != null && i <= k)
                    {
                        r = q;
                        q = p;
                        p = p.Next;
                        q.Next = r;
                        i++;
                    }

                    returnNode = q;
                    nextStart = p;
                }
                if (currentTail != null)
                    currentTail.Next = SkipAndReverseRecursive(nextStart, k, !isSkip);
            }
            return returnNode;
        }
        public static void SkipAndReverse(JabzLLNode<int> start, int k)
        {
            JabzLLNode<int> temp = start;
            int i = 1;
            while (i < k)
            {
                temp = temp.Next;
                i++;
            }

            JabzLLNode<int> p = temp.Next;
            JabzLLNode<int> q = null;
            JabzLLNode<int> r = null;

            while (p != null)
            {
                r = q;
                q = p;
                p = p.Next;
                q.Next = r;
            }

            temp.Next = q;
        }

        public static JabzLLNode<int> FindElementFromEnd(JabzLLNode<int> start, int k)
        {
            JabzLLNode<int> P1 = start;
            int i = 1;
            while (i < k)
            {
                P1 = P1.Next;
                i++;
            }
            var P2 = start;
            while (P1.Next != null)
            {
                P1 = P1.Next;
                P2 = P2.Next;
            }

            return P2;
        }
    }
   

}
