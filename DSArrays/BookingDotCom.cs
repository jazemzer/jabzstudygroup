﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSArrays
{
    class Person
    {
        public string Name { get; set; }
        public int Score { get; set; }
    }
    class BookingDotCom
    {
        public static void PracticeProblems()
        {
            ChainingNames();
            Console.WriteLine(Convert.ToByte("FF", 16));
            //Console.WriteLine(Convert.ToString(24, 2));
            //Conver to hex and binary
            var test = ConvertToBinary(23);
            test = ConvertToBinary(3);
            test = ConvertToBinary(8);
            FindUniqueItem();
            CustomObjects();
            Histogram();
            SymmetricDifference();
            RepeatingElementInAtleastTwoArrays();
            RepeatingElementInThreeArrays();
            TwoRepeatingElementsInArray();
        }
    

        static void ChainingNames()
        {
            String[] input = {"Luis", "Hector", "Selena", "Emmanuel", "Amish"};

            Dictionary<char, List<int>> startingLetter = new Dictionary<char, List<int>>();
            Dictionary<char, List<int>> endingLetter = new Dictionary<char, List<int>>();

            
            for (int i =0; i < input.Length; i++)
            {
                var temp = input[i].ToLower();
                var startChar = temp[0];
                var endChar = temp[temp.Length - 1];

                #region Create a adjacency list for starting characters
                if (startingLetter.ContainsKey(startChar))
                {
                    var list = startingLetter[startChar];
                    list.Add(i);
                    startingLetter[startChar] = list;
                }
                else
                {
                    startingLetter.Add(startChar,new List<int>(){i});
                }
                #endregion

                #region Create a adjacency list for ending characters
                if (endingLetter.ContainsKey(endChar))
                {
                    var list = endingLetter[endChar];
                    list.Add(i);
                    endingLetter[endChar] = list;
                }
                else
                {
                    endingLetter.Add(endChar, new List<int>() { i });
                }
                #endregion
            }

            #region Find if its a valid input

            var possibleStarts = new Dictionary<char, int>();
            var possibleEnds = new Dictionary<char, int>();

            
            for(int i = 97; i < 123; i++)
            {
                var startCount = 0;
                var endCount = 0;

                var currentAlphabet = Convert.ToChar(i);
                if (startingLetter.ContainsKey(currentAlphabet))
                {
                    startCount = startingLetter[currentAlphabet].Count;
                }

                if (endingLetter.ContainsKey(currentAlphabet))
                {
                    endCount = endingLetter[currentAlphabet].Count;
                }

                var diff = startCount - endCount;
                if (diff < 0)
                {
                    possibleEnds.Add(currentAlphabet,-diff);
                }
                else if (diff > 0)
                {
                    possibleStarts.Add(currentAlphabet, diff);
                }
            }

            var startSum = possibleStarts.Sum(x => x.Value);
            var endSum = possibleEnds.Sum(x => x.Value);


            if (startSum > 1 || endSum > 1)
            {
                Console.WriteLine("Invalid Input");
                return;
            }

            #endregion

            var startingChar = possibleStarts.Keys.First();



        }

        static string ConvertToBinary(int number)
        {
            if (number > 1)
            {
                var remainder = 0;                
                return ConvertToBinary(number/2) + ((number%2 == 0) ? "0" : "1");                
            }

            return number.ToString();
        }

        static void FindUniqueItem()
        {
            int[] input = new int[] { 1, 1, 2, 3, 56, 4, 7, 4, 3, 2, 56, 1000 };

            var freqTable = new Dictionary<int, int>();
            foreach (var i in input)
            {
                if (freqTable.ContainsKey(i))
                {
                    freqTable[i] += 1;
                }
                else
                {
                    freqTable.Add(i, 1);
                }
            }

            var result = freqTable.Where(x => x.Value == 1).Select(s => s.Key).ToList();

            Console.WriteLine(result.FirstOrDefault());

        }

        public static void CustomObjects()
        {
            List<Person> people = new List<Person>()
            {
                new Person(){Name = "John", Score = 5},
                new Person(){Name = "Bob", Score = 1},
                new Person(){Name = "Carlos", Score = 5},
                new Person(){Name = "Alilce", Score = 5},
                new Person(){Name = "Ziglar", Score = 10},
                new Person(){Name = "Donald", Score = 7},
            };

            var result = people.OrderByDescending(x => x.Score).ThenBy(x => x.Name);

            foreach (var item in result)
            {
                Console.WriteLine(item.Name + " , " + item.Score);
            }
        }

        private static void Histogram()
        {
            string[] input = new string[]
            {
                "abacus",
                "abaculus",
                "Abadite",
                "basic",
                "trial",
                "Killer"
            };

            Dictionary<char, int> freq = new Dictionary<char, int>();

            foreach (string s in input)
            {
                foreach (char c in s.ToLower())
                {
                    if (freq.ContainsKey(c))
                    {
                        freq[c] += 1;
                    }
                    else
                    {
                        freq.Add(c, 1);
                    }
                }
            }

            var sorted = freq.OrderBy(x => x.Key);
            foreach (var kvPair in sorted)
            {
                for (int i = 0; i < kvPair.Value; i++)
                {
                    Console.Write("*");
                }
                Console.Write(kvPair.Key);
                Console.WriteLine();

            }

            int max = freq.Max(x => x.Value);
            foreach (var kvPair in sorted)
            {
                int scaled = (kvPair.Value * 79)/max;
                for (int i = 0; i < scaled; i++)
                {
                    Console.Write("*");
                }
                Console.Write(kvPair.Key);
                Console.WriteLine();

            }

            Console.WriteLine();
        }

        private static void TwoRepeatingElementsInArray()
        {
            int[] arr = {1, 3, 4, 5, 2, 3, 1};
            int size = arr.Length;

            int xor = arr[0]; /* Will hold xor of all elements */
            int set_bit_no;  /* Will have only single set bit of xor */
            int i;
            int n = size - 2;
            int x = 0, y = 0;

            /* Get the xor of all elements in arr[] and {1, 2 .. n} */
            for (i = 1; i < size; i++)
                xor ^= arr[i];
            for (i = 1; i <= n; i++)
                xor ^= i;

            /* Get the rightmost set bit in set_bit_no */
            set_bit_no = xor & ~(xor - 1);

            /* Now divide elements in two sets by comparing rightmost set
             bit of xor with bit at same position in each element. */
            for (i = 0; i < size; i++)
            {
                if ((arr[i] & set_bit_no) == set_bit_no)
                    x = x ^ arr[i]; /*XOR of first set in arr[] */
                else
                    y = y ^ arr[i]; /*XOR of second set in arr[] */
            }
            for (i = 1; i <= n; i++)
            {
                if ((i & set_bit_no )== set_bit_no)
                    x = x ^ i; /*XOR of first set in arr[] and {1, 2, ...n }*/
                else
                    y = y ^ i; /*XOR of second set in arr[] and {1, 2, ...n } */
            }
            Console.WriteLine("X and y are {0}, {1}", x, y);
        }

        private static void RepeatingElementInAtleastTwoArrays()
        {
            int[] setA = new int[] { 1, 2, 3, 4, 5, 6 };
            int[] setB = new int[] { 4, 5, 6, 6,8 };
            int[] setC = new int[] { 5, 7,7,7,8 };

            Dictionary<int, Byte> freqCount = new Dictionary<int, Byte>();
            for (int i = 0; i < setA.Length; i++)
            {
                if (!freqCount.ContainsKey(setA[i]))
                    freqCount.Add(setA[i], 1);
            }
            for (int i = 0; i < setB.Length; i++)
            {
                if (!freqCount.ContainsKey(setB[i]))
                    freqCount.Add(setB[i], 2);
                else
                    freqCount[setB[i]] |= 2;
            }
            for (int i = 0; i < setC.Length; i++)
            {
                if (!freqCount.ContainsKey(setC[i]))
                    freqCount.Add(setC[i], 4);
                else
                    freqCount[setC[i]] |= 4;
            }

            foreach (var item in freqCount)
            {
                if (item.Value == 3 || item.Value == 5 || item.Value == 6 || item.Value == 7)
                {
                    Console.Write(item.Key + " ");
                }
            }
            Console.WriteLine();
        }

        private static void RepeatingElementInThreeArrays()
        {
            int[] setA = new int[] { 1, 2, 3, 4, 5, 6 };
            int[] setB = new int[] { 4, 5, 6, 6 };
            int[] setC = new int[] { 4, 5, 7, 7, 7 };

            Dictionary<int, Byte> freqCount = new Dictionary<int, Byte>();
            for (int i = 0; i < setA.Length; i++)
            {
                if (!freqCount.ContainsKey(setA[i]))
                    freqCount.Add(setA[i], 1);
            }
            for (int i = 0; i < setB.Length; i++)
            {
                if (!freqCount.ContainsKey(setB[i]))
                    freqCount.Add(setB[i], 2);
                else
                    freqCount[setB[i]] |= 2;
            }
            for (int i = 0; i < setC.Length; i++)
            {
                if (!freqCount.ContainsKey(setC[i]))
                    freqCount.Add(setC[i], 4);
                else
                    freqCount[setC[i]] |= 4;
            }

            foreach (var item in freqCount)
            {
                if (item.Value == 7)
                {
                    Console.Write(item.Key + " ");
                }
            }
            Console.WriteLine();
        }

        private static void SymmetricDifference()
        {
            int[] setA = new int[] { 1, 2, 3, 4, 5 };
            int[] setB = new int[] { 4, 5, 6, 6 };

            HashSet<int> output = new HashSet<int>();
            Dictionary<int, int> freqCount = new Dictionary<int, int>();

            for (int i = 0; i < setA.Length; i++)
            {
                if (!freqCount.ContainsKey(setA[i]))
                {
                    freqCount.Add(setA[i], 1);
                }
            }

            for (int i = 0; i < setB.Length; i++)
            {
                if (freqCount.ContainsKey(setB[i]))
                {
                    freqCount[setB[i]] += 1;
                }
                else
                {
                    output.Add(setB[i]);
                }
            }

            var itemsFromSetA = freqCount.Where(x => x.Value == 1).Select(x => x.Key).ToList();
            foreach (var item in itemsFromSetA)
            {
                output.Add(item);
            }

            foreach (var item in output)
            {
                Console.Write(item + " ");
            }

            Console.WriteLine();
        }
    }
}
