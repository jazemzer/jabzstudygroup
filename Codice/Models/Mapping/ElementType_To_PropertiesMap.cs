using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Codice.Models.Mapping
{
    public class ElementType_To_PropertiesMap : EntityTypeConfiguration<ElementType_To_Properties>
    {
        public ElementType_To_PropertiesMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ElementType, t.PropertyCode });

            // Properties
            this.Property(t => t.ElementType)
                .IsRequired()
                .HasMaxLength(5);

            this.Property(t => t.PropertyCode)
                .IsRequired()
                .HasMaxLength(5);

            // Table & Column Mappings
            this.ToTable("ElementType_To_Properties");
            this.Property(t => t.ElementType).HasColumnName("ElementType");
            this.Property(t => t.PropertyCode).HasColumnName("PropertyCode");
        }
    }
}
