﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Codice.Models;

namespace Codice
{
    class Program
    {
        static void Main(string[] args)
        {
            var codiceRepository = new CodiceRepository();
            var findByUniqueId = codiceRepository.FindById(1);
            var findByType = codiceRepository.FindByType("T1");
            var findByTypeAndProperty = codiceRepository.FindByTypeAndPropertyInfo("T1", "P1", "5");
              

            var dummyTask = new DummyTask();
            int counter1 = dummyTask.CreateTasks(10); //2000000);

            Console.ReadLine();

        }
    }

    public class DummyTask
    {
        object locker = new object();
        private int counter = 0;

        int Counter
        {
            get
            {
                int result;
                lock (locker)
                {
                    result = counter;
                }

                    return result;
            }
            set
            {
                lock (locker)
                {
                    counter = value;
                    
                }
            }
        }

        private const int MAX_VALUE = 10;

        void DoSomething()
        {
            Random random = new Random();
            Thread.CurrentThread.Join(random.Next(MAX_VALUE));

        }

        void DoWork()
        {
            int value = 0;
            lock (locker)
            {
                value = Counter;
            
            DoSomething();

                Counter = value + 1;
            }
        }

        public int CreateTasks(int tasksNumber)
        {
            Counter = 0;
            Task[] tasks = new Task[tasksNumber];

            Parallel.For(0, tasksNumber, x => DoWork());
            //for (int i = 0; i < tasksNumber; i++)
            //{
            //    tasks[i] = Task.Factory.StartNew(() => DoWork());
            //}
           // Task.WaitAll(tasks);
            return Counter;
        }
    }
}
