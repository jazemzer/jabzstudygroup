using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Codice.Models.Mapping
{
    public class Element_PropertiesMap : EntityTypeConfiguration<Element_Properties>
    {
        public Element_PropertiesMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ElementId, t.PropertyCode });

            // Properties
            this.Property(t => t.ElementId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.PropertyCode)
                .IsRequired()
                .HasMaxLength(5);

            this.Property(t => t.PropertyValue)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("Element_Properties");
            this.Property(t => t.ElementId).HasColumnName("ElementId");
            this.Property(t => t.PropertyCode).HasColumnName("PropertyCode");
            this.Property(t => t.PropertyValue).HasColumnName("PropertyValue");

            // Relationships
            this.HasRequired(t => t.Element_Info)
                .WithMany(t => t.Element_Properties)
                .HasForeignKey(d => d.ElementId);

        }
    }
}
