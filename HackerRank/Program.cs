﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HackerRank.Algorithms;
using HackerRank.Contests.Week11;
using HackerRank.Search;
using HackerRank.Warmup;

namespace HackerRank
{
    class Program
    {
        static void Main(string[] args)
        {
            StrangeNumbers.Code();
        }
    }
}
