using System;
using System.Collections.Generic;

namespace Codice.Models
{
    public partial class Element_Properties
    {
        public int ElementId { get; set; }
        public string PropertyCode { get; set; }
        public string PropertyValue { get; set; }
        public virtual Element_Info Element_Info { get; set; }
    }
}
