﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataStructures;

namespace Logic
{
    public class GraphClient
    {
        public static void Code()
        {
            //Graph graph = new Graph(@"D:\Learning\JabsStudyGroup\Logic\Data\tinyG.txt");

            //DepthFirstSearch search = new DepthFirstSearch(graph, 7);
            //for (int v = 0; v < graph.V; v++)
            //{
            //    if (search.IsConnected(v))
            //    {
            //        Console.Write(v + " ");
            //    }
            //}
            //Console.WriteLine();


            #region Tiny Connected

            var graph = new Graph(@"D:\Learning\JabsStudyGroup\Logic\Data\tinyCG.txt");

            DepthFirstSearch search = new DepthFirstSearch(graph, 0);
            for (int v = 0; v < graph.V; v++)
            {
                if (search.IsConnected(v))
                {
                    Console.Write(v + " ");
                }
            }
            Console.WriteLine();

            foreach (var path in search.PathTo(5))
            {
                Console.Write(path + " ");
            }
            Console.WriteLine();
            #endregion

            Console.ReadLine();
        }
    }
}
