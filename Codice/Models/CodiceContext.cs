using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Codice.Models.Mapping;

namespace Codice.Models
{
    public partial class CodiceContext : DbContext
    {
        static CodiceContext()
        {
            Database.SetInitializer<CodiceContext>(null);
        }

        public CodiceContext()
            : base("Name=CodiceContext")
        {
        }

        public DbSet<Element_Info> Element_Info { get; set; }
        public DbSet<Element_Properties> Element_Properties { get; set; }
        public DbSet<ElementType_To_Properties> ElementType_To_Properties { get; set; }
        public DbSet<Property_Info> Property_Info { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new Element_InfoMap());
            modelBuilder.Configurations.Add(new Element_PropertiesMap());
            modelBuilder.Configurations.Add(new ElementType_To_PropertiesMap());
            modelBuilder.Configurations.Add(new Property_InfoMap());
        }
    }
}
