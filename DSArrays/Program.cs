﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSArrays
{
    class Program
    {



        static int Fibb(int n)
        {
            if (n < 2)
                return 1;

            return Fibb(n -1 ) + Fibb(n -2 );
        }
        static void Main(string[] args)
        {
            Dijkstra.Implementation();
            BookingDotCom.PracticeProblems();
            //FindSubsequenceThatAddsToGivenSum.Implementation();
            Console.Read();
            return;

            Console.WriteLine(Fibb(7));

            List<int> listA = new List<int>() { 1, 2, 3, 4 };
            List<int> listB = new List<int>() { 3, 4, 5, 6 };
            
            foreach (var i in listA.Except(listB))
            {
                Console.WriteLine(i);
            }

            HashSet<int> hashSet = new HashSet<int>(listA);
            foreach (var item in listB)
            {
                hashSet.Add(item);
            }

            Console.WriteLine(5 - 1 >> 1);
            int xor2 = 3;
            Console.WriteLine(xor2 & ~(xor2 - 1));

            byte b = 2;
            //Console.WriteLine(Convert.ToString(~b,2));
            Console.WriteLine(Convert.ToString(10,2));
            Console.WriteLine(Convert.ToString(-10, 2));
            Console.WriteLine(3 & 1);
            //Console.WriteLine(Convert.ToString(10 <<1,2));
            
            Console.WriteLine(Convert.ToByte("FF", 16));

            FindMaxConsecutiveseries();
            FindConsecutiveNumbersThatAddToN();
            //maxSeqMahesh();


            var input1 = new[] { 5, 0, 9, 2, 5, 5, 5 };
            var input2 = new[] { 10 };
            var input3 = new[] { -20, 0, 30, 0, 5 };
            var input4 = new[] { -20, -7, 35, 0, 2, 0, 5 };
            var input5 = new[] { 5, 5, -6, 6, 0, 0 };

            PrintSubArrayWithGivenSum(input1, 10);
            PrintSubArrayWithGivenSum(input2, 10);
            PrintSubArrayWithGivenSum(input3, 10);
            PrintSubArrayWithGivenSum(input4, 10);
            PrintSubArrayWithGivenSum(input5, 10);
            Console.Read();
        }

        private static void PrintSubArrayWithGivenSum(int[] a, int k)
        {
            // represents <sum fromstart uptpthat index, index> 
            var hashMap = new Dictionary<int, int>();
            hashMap.Add(0, -1);

            int currSum = 0;

            for (int i = 0; i < a.Length; i++)
            {
                currSum += a[i];

                if (hashMap.ContainsKey(currSum - k))
                {
                    var index = hashMap[currSum - k];
                    Console.WriteLine(" Sub array that sums upto {0} start from index {1} and ends at index {2}", k, index + 1, i);
                    break;
                }
                if (!hashMap.ContainsKey(currSum))
                    hashMap.Add(currSum, i);
            }
        }

        static void FindConsecutiveNumbersThatAddToN()
        {
            //int[] input = new[] { 5, 0, 9, 2, 5, 5, 5 };
            //int[] input = new[] { 10};
            //int[] input = new[] { -20, 0, 30, 0, 5};
            //int[] input = new[] { -20, -7, 35, 0, 2, 0, 5};
            //int[] input = new[] { 5, 5, -6, 6, 0, 0 };
            int[] input = new[] { 5, 2, -6, 0, 12, -2 };
            
            int sum = 0;
            int expectedSum = 10;
            int start = 0;
            int end = -1;
            int firstItem = 0;
            
            for (int i = 0; i < input.Length; i++)
            {
                sum += input[i];
                
                if (sum > expectedSum
                    && end == -1)
                {
                    do
                    {
                        firstItem = input[start];
                        sum -= firstItem;
                        start++;
                    } while (sum > expectedSum);
                }
                if (sum == expectedSum)
                {
                    end = i;
                }
            }
            if (end != -1)
            {
                var result = input.Skip(start).Take(end + 1 - start);
                Console.WriteLine("First Matching Series " + string.Join(",", result));
            }
        }

        static void maxSeqMahesh()
        {
            var numbers = new int[] { -20, -7, 35, 0, 2, 0, 5 };
            var sum = 0;

            foreach (var number in numbers)
            {
                sum += number;
            }
            int i = 0, j = numbers.Length - 1;
            while (i < j)
            {
                if (numbers[i] < 0)
                {
                    sum = sum - numbers[i];
                    i++;
                }
                if (numbers[j] < 0)
                {
                    sum = sum - numbers[i];
                    j--;
                }

                if (numbers[i] > 0 && numbers[j] > 0)
                {
                    break;
                }
            }
            while (i <= j)
            {
                Console.Write(numbers[i] + ",");
                i++;
            }
            Console.WriteLine();
            Console.WriteLine(sum);
        }

        static void FindMaxConsecutiveseries()
        {
            //var input = new[] {1, 2, -6, 4, -2, 8, -5};
             var input = new[]  {-2, -3, 4, -1, -2, 1, 5, -3}; 
            //var input = new[] {-1, -7, 5, 0, 9, -10, 5, 0,-20,0, 2, 5, 5, 5, 0, 0 };

            int tempSum = 0;
            int maxSum = 0;
            int start = 0;
            int end = -1;
            ;
            for (int i = 0; i < input.Length; i++)
            {
                tempSum += input[i];
                if (tempSum < 0)
                {
                    tempSum = 0;
                    start = i + 1;
                    end = -1;
                }
                else
                {
                    if (tempSum > maxSum)
                    {
                        maxSum = tempSum;
                        end = i;
                    }
                    else if (tempSum == maxSum)
                    {
                        end = i;
                    }

                }
            }

            if (end != -1)
            {
                var result = input.Skip(start).Take(end + 1 - start);
                Console.WriteLine("Max " + maxSum + " : " + string.Join(",", result));
            }

        }

      

       
    
    }
}
