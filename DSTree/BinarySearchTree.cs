﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSTree
{
    class Node
    {
        public int Data { get; set; }
        public Node Left { get; set; }
        public Node Right { get; set; }
    }

    class BST
    {
        public Node Root { get; set; }

        public BST()
        {
            Root = null;
        }

      
        // Depth-first traversal is classified by position of the root element with regard to the left and right nodes. 
        // root node could be placed to the left of the left node (pre-order), 
        // between the left and right node (in-order), 
        // or to the right of the right node (post-order).

        public void PreOrder(Node root)
        {
            if (root != null)
            {
                Console.WriteLine(root.Data);
                PreOrder(root.Left);
                PreOrder(root.Right);
            }
        }

        public void InOrder(Node root)
        {
            if (root != null)
            {
                InOrder(root.Left);
                Console.WriteLine(root.Data);
                InOrder(root.Right);
            }
        }

        public void PostOrder(Node root)
        {
            if (root != null)
            {
                InOrder(root.Left);
                InOrder(root.Right);
                Console.WriteLine(root.Data);

            }
        }

        public void Insert(int i)
        {
            var newNode = new Node();
            newNode.Data = i;

            if (Root == null)
            {
                Root = newNode;
                return; 
            }
            else
            {
                var current = Root;
                Node parent = null;
                while (true)
                {
                    parent = current;
                    if (i < current.Data)
                    {
                        current = current.Left;
                        if (current == null)
                        {
                            parent.Left = newNode;
                            break;
                        }
                    }
                    else
                    {
                        current = current.Right;
                        if (current == null)
                        {
                            parent.Right = newNode;
                            break;
                        }
                    }
                }
            }
        }

        public int FindMin()
        {
            Node current = Root;
            while (current.Left != null)
                current = current.Left;
            return current.Data;
        }

        public int FindMax()
        {
            Node current = Root;
            while (current.Right != null)
                current = current.Right;
            return current.Data;
        }

        public Node Find(int data)
        {
            Node current = Root;

            while (current.Data != data)
            {
                if (data < current.Data)
                {
                    current = current.Left;
                }
                else
                {
                    current = current.Right;
                }

                if (current == null)
                {
                    return null;
                }
            }

            return current;
        }

        public void Delete(Node delNode)
        {
            /* Logic
             * Find the node to be deleted
             * If delete node has no children
             *      * If its a root node, set root as null
             *      * if it is a left child, set its parents left child to null
             *      * If it is a right child, set its parent's right child to null
             * If delete node has one children
             *      * and the child of delnode is a leftchild
             *          * If Root, promote left child to root
             *          * IF the delNode is a left child, set parent's left as the delNodes left
             *          * If delNode is right child, set parent's right as delNodes left
             *      * and the child of delnode is a right child
             *          * If Root, promote right child to root
             *          * If delnode is a leftchild, set parent's left as delnodes right
             *          * If delnode is a rightchild, set parent's set parent's right as delnodes right
             * If delNode has two children
             *      * Find successor - the smallest node that is larger than the node to be deleted.. 
             *          * Found as the leftmost node in the delnode's right child
             *      * If Successor has right child 
             *          * Set succesorParent's leftchild as succesor's right
             *      * Succesor cannot have left child 
             *      * Set succesor's right as delNode's right child
             *      * Set succesor's left as delNode's left 
             *      * If delnode is root, promote successor as root
             *      * If delnode is leftchild, set parent's left as succesor
             *      * If delnode is rightchild, set parent's right as succesor
             *          
             */
        }
    }

    class BinarySearchTree
    {
        public static void Implementation()
        {
            
        }
    }
}
