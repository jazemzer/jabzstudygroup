﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSStacks
{
    public class JabzStack<T>
    {
        List<T> dataCollection = new List<T>();

        private int top = -1;
        
        public T Pop()
        {

            var data = dataCollection[top];
            dataCollection.RemoveAt(top);
            top -= 1;
            return data;
        }

        public void Push(T data)
        {
            dataCollection.Add(data);
            top += 1;
        }
    }
}
